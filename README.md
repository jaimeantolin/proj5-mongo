# Project 5: Brevet time calculator with Ajax and MongoDB
By: Jaime Antolin Merino

Simple list of controle times from project 4 stored in MongoDB database.

## Users

In order to use this calculator effectively 4 inputs are required from you:
	
* Distance: using the drop down you must select the total distance of your brevet (it is okay if your total distance is a bit longer, just pick the closest one we will discuss this later)
* Start date: The date on when you plan to start your brevet
* Start time: The start time of when you plan to start the brevet
* Location of controls: in each row of the calculator you can either enter a distance in km or miles (translation will be computed automatically) 

Your open and close times will now automatically appear in the cells of the same row. The notes column is to specify error messages.

When submit is pressed current data will be stored in a database and page will be refreshed to allow calculating another brevet. When display button is pressed the last stored brevet will be displayed in a new page with all its controls and necessary data.

Rules:

* Final control cannot be further than 20% of the total distance of the brevet indicated. If it is error message will be displayed for that control.
* Negative controle distances will launch an error
* Controls in the first 60km will be calculated differently and are not encouraged (to see how times are calculated check developer section).


## Functionality added from proj4

To undertand prior functionaliry look at project 4 readme nothing not mentioned here hasnt changed.

 * 1) Create two buttons ("Submit") and ("Display") in the page where have controle times. 
 * 2) On clicking the Submit button, the control times should be entered into the database. 
 * 3) On clicking the Display button, the entries from the database should be displayed in a new page.
 * 4) Negative numbers error caught and handled
 * 5) Enter does not submit form to avoid error notifications popping up before data has been entered 

## Error cases

* EMPTY TABLE - If the user tries to submit an empty table they will be redirected to an error page that specifies their issue and a link will be provided to return to the calculator.

* EMPTY DB - If the user tries to display the information stored in the db but it is empty, they will be redirected to an error page that specifies their issue and a link will be provided to return to the calculator.

* ERROR in controle distances - If the user tries to submit data that contains a previously caught error(negative numbers or last control 20% further away than brevet distance), they will be redirected to an error page that specifies their issue and a link will be provided to return to the calculator.

* MULTIPLE CLICKS - The submit button clears the db every time it is pressed before inserting new data to avoid mixing brevets and also refreshes the page which resets all values before allowing other data to be submitted. So the data displayed will always be the latest data that has been calculated.

* ENTER KEY PRESS - Traditionally the enter key submits data in a form and as we are treating our table as a form, the enter key has been suppresed to avoid submitting our data before we are finished entering all controls and to avoid the page throwing an error when we enter the first data since display button is also triggered and there would be no data in the db.

