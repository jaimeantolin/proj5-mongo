"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import os
from pymongo import MongoClient

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.controles

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    brevet_dist = request.args.get("brevet_dist", type=int)
    brevet_start = arrow.get(request.args.get("brevet_start", type=str))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, brevet_dist, brevet_start)
    close_time = acp_times.close_time(km, brevet_dist, brevet_start)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route("/_submit", methods=['POST'])
def _submit():
    db.controles.remove()

    location_values = request.form.getlist("location")
    km_values = request.form.getlist("km")
    start_values = request.form.getlist("open")
    close_values = request.form.getlist("close")

    counter = 0
    for x in start_values:
        if x == "ERROR":
            return flask.render_template('error.html')
        elif x != "":
            counter +=1

    if counter == 0:
        return flask.render_template('emptytable.html')        


    for i in range(counter):
        item_doc = {
            'brevet_dist': request.form['distance'],
            'location': location_values[i],
            'km': km_values[i],
            'start_time': start_values[i],
            'close_time': close_values[i]
        }
        db.controles.insert_one(item_doc)

    return flask.redirect(flask.url_for('index'))

#############

@app.route('/_display', methods=['POST'])
def _display():

    _items = db.controles.find()

    if _items.count() == 0:
        return flask.render_template('emptydb.html')  

    items = [item for item in _items]

    return flask.render_template('display.html', items=items)

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
